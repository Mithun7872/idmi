<?php
include '../../library/dbconnect.php';
$gen=$_POST['appno'];
$appid=$_POST['fileno'];
 $query4="SELECT * FROM input_details WHERE Fileno='$gen' and AppId='$appid'";

$result4=mysql_query($query4)or die(mysql_error());
$row=mysql_fetch_array($result4);
$dis=$row['districts'];
$query3="Select rev_district_name from rev_district_master where rev_district_code='$dis'";
$result3=mysql_query($query3)or die(mysql_error());
$row1=mysql_fetch_array($result3);
$districtss=$row1['rev_district_name'];
// Include the main TCPDF library (search for installation path).
require_once('tcpdf_include.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle('TCPDF Example 006');
$pdf->SetSubject('TCPDF Tutorial');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
// $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 006', PDF_HEADER_STRING);

// set header and footer fonts
// $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
// $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
// $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
// $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
// $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
// if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	// require_once(dirname(__FILE__).'/lang/eng.php');
	// $pdf->setLanguageArray($l);
// }

// ---------------------------------------------------------

// set font
$pdf->SetFont('dejavusans', '', 10);

// add a page
$pdf->AddPage();

// writeHTML($html, $ln=true, $fill=false, $reseth=false, $cell=false, $align='')
// writeHTMLCell($w, $h, $x, $y, $html='', $border=0, $ln=0, $fill=0, $reseth=true, $align='', $autopadding=true)

// create some HTML content
$html = '<div>
				 <div style="text-align: center">
				 	PART-II<br />
					(RECOMMENDATION OF THE STATE GRANT -IN -AID COMMITTEE)
				 </div>
				 <div style="margin-top: 20px;">
				 &nbsp;&nbsp;Name of the Organization Whose case is being recommended: <u>'.$row['orgname'].'</u>
				 </div>				
				<div>
					<table>
					<tr>
						<td width="20">1.</td>
						<td width="500">Whether State  govt has drawn up & disseminated a criteria for prioritization of applications under this scheme</td>
						<td  width="30">:</td>
						<td>Yes/No</td>
					</tr>
					<br />	
					<tr>
						<td>2.</td>	
						<td>Whether the proposal being recommended  for financial assistance,is in accordance with this criteria</td>
						<td  width="30">:</td>
						<td>Yes/No</td>
					</tr>	
					<br />
					<tr>	
						<td>3.</td>
						<td>Whether proposal has been received in the specified application form as prescribed?</td>					
						<td  width="30">:</td>
						<td>Yes/No</td>
					</tr>	
					<br />
					<tr>
						<td>4.</td>
						<td>Whether proposal has been scurtinized  and is in accordance with the eligibility and financial parameters of the scheme</td>
						<td  width="30">:</td>
						<td>Yes/No</td>
					</tr>	
					<br />
					<tr>	
						<td>5.</td>
						<td>Whether the organization has a legal rights to land on which infrastructure is being proposed under this scheme?</td>
						<td  width="30">:</td>
						<td>Yes/No</td>
					</tr>	
					<br />
					<tr>	
						<td>6.</td>
						<td>Whether estimates for the proposal infrastructure are not more than the state PWD schedule of rates?</td>
						<td  width="30">:</td>
						<td>Yes/No</td>
					</tr>	
					<br />
					<tr>	
						<td>7.</td>
						<td>Whether it has been ascertained that the organization being recommended for funding is not duplicating funds received from other state/Central Govt.scheme/programmes for the same purpose?</td> 
						<td  width="30">:</td>
						<td>Yes/No</td>
					</tr>	
					<br />
					<tr>	
						<td>8.</td>
						<td>Whether the organization whose case is being recommended,has the furnished the audited accounts,utilization certificates,annual report & any other performance report as specified,which was due till date of forwarding of case?</td>
						<td  width="30">:</td>
						<td>Yes/No</td>
					</tr>
					<br />	
			
					<tr>
					<td>9.</td>
					<td>The Order of Priority in Which the case of the organization is being recomemended?</td>
					<td  width="30">:</td>
						<td>Yes/No</td>
					</tr>
					
					
					</table><p></p>
				The application has benn examined and it is certified that the organization is elligible<br />for assistance and has the capability of taking up a programme applied for:.	
				
				<p align="right">(Signature of the Member Secretary of State GIAC)</p>
				
				
				
				</div>
			</div>	';

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');


$pdf->lastPage();	

// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('print.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
