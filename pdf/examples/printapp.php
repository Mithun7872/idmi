<?php
include '../../library/dbconnect.php';
$gen=$_POST['appno'];
$appid=$_POST['fileno'];
 $query4="SELECT * FROM input_details WHERE Fileno='$gen' and AppId='$appid'";

$result4=mysql_query($query4)or die(mysql_error());
$row=mysql_fetch_array($result4);
$dis=$row['districts'];
$query3="Select rev_district_name from rev_district_master where rev_district_code='$dis'";
$result3=mysql_query($query3)or die(mysql_error());
$row1=mysql_fetch_array($result3);
$districtss=$row1['rev_district_name'];
// Include the main TCPDF library (search for installation path).
require_once('tcpdf_include.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle('TCPDF Example 006');
$pdf->SetSubject('TCPDF Tutorial');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
// $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 006', PDF_HEADER_STRING);

// set header and footer fonts
// $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
// $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
// $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
// $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
// $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
// if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	// require_once(dirname(__FILE__).'/lang/eng.php');
	// $pdf->setLanguageArray($l);
// }

// ---------------------------------------------------------

// set font
$pdf->SetFont('dejavusans', '', 10);

// add a page
$pdf->AddPage();

// writeHTML($html, $ln=true, $fill=false, $reseth=false, $cell=false, $align='')
// writeHTMLCell($w, $h, $x, $y, $html='', $border=0, $ln=0, $fill=0, $reseth=true, $align='', $autopadding=true)

// create some HTML content
$html = '<div>
<div float:left>
<span style="text-align:left">Application id:'.$row['Fileno'].'</span>
<div>
<span style="text-align:right">Application no:'.$row['AppId'].'</span>
</div>
</div>

<div style="font:bold;text-align:center;font-size:16px;">
SCHEME FOR DEVELOPMENT OF INFRASTRUCTURE IN <br />MINORITY EDUCATIONAL INSTITUTIONS OF<br /> ELEMENTARY/SECONDARY AND HIGHER SECONDARY LEVEL
<br />
<span style="font-weight:bold";text-align: center;>Application Form</span>
</div>
<div id="content">

<table style="">
	
	<tr valign="top">
		<td style="width:5%;">1.</td>
		<td style="width:40%;">Revenue district</td>
		<td style="width:1%;">:</td>
		<td style="width:43%;">'.$districtss.'</td>
	</tr>
<br />
    <tr>
    	<td>2.</td>
        <td>Name of Voluntary Organisation/Society <br /> managing the school(With complete address):</td>
        <td>:</td>
        <td>'.$row['orgname'].'</td>
    </tr>
    <br />
	<tr valign="top">
		<td>3.</td>
		<td>School code</td>
		<td valign="center">:</td>
		<td>'.$row['code'].'</td>
	</tr>
	
    <br />
	<tr>
		<td>4.</td>
        <td>Name with address of the school/institution for financial assistance required</td>	
        <td>:</td>
        <td>'.$row['schoolname'].'</td>
    </tr>
    <br />
    <tr>
    	<td>5.</td>
    	<td>Objects and activities(give brief history of the organisation/society managing the school:)</td>
        <td>:</td>
        <td>'.$row['org_history'].'</td>
    </tr>
    <br />
    <tr>
    	<td>6.</td>
    	<td>Specific activities of the school for which financial <br /> assistance is sought under the scheme</td>
        <td>:</td>
        <td>'.$row['school_activities'].'</td>
    </tr>
    <br />
    <tr>
    	<td>7.</td>
    	<td>Whether registered under the Centra/State Board?<br />If Yes,Registration No.(A copy of	the registration certificate to be enclosed): </td>
        <td>:</td>
        <td>'.$row['registration'].'</td>
    </tr>
    <br />
    <tr>
    	<td>8.</td>
    	<td>Organisational structure,total staff.their roles and responsibilities,<br />staff turnover of educational <br />institute/school for which assistance is being<br />             sought and the voluntary organisation/society</td>
        <td>:</td>
        <td>'.$row['org_details'].'</td>
    </tr>
    <br />
    <tr  valign="top">
    	<td>9.</td>
    	<td>Governing Board/Managing Committe-number of <br />members,their role,meeting held and attendance,<br />their involvement in decision making of<br /> educational             institution/school and the voluntary<br /> organisation/society concerned(List of             members <br />may be enclosed)</td>
        <td>:</td>
        <td>'.$row['Govrning_board'].'</td>
    </tr>
    <br />
    <tr>
    	<td>10.</td>
    	<td>Name and address of bankers,auditors,legal<br /> advisors(including details of accounts)of voluntary organisation/society</td>
        <td>:</td>
        <td>'.$row['bankers'].'</td>
    </tr>
    <br />
    <tr>
    	<td>11.</td>
    	<td>Details of infra-structural facilities availible with<br /> educational institution/school for whom,<br /> assistance is being sought</td>
    	<td>:</td>
    	<td>'.$row['infra_details'].'</td>	
    </tr>
    <br />		
     <br />		
      <br />		 <br />		
    		<tr>
    		<td colspan="4">
    		
 			<table style="padding:0 60px;">
 			<br />
 			<tr>
	 			<td>Whether the building is rented or own?:<br></td>
				<td>'.$row['rented'].'</td> 
			</tr>
			<br />
			<tr>
				<td>No. of rooms availible for classes and<br /> Administrative purposes<br></td>
				<td>'.$row['classroom'].'</td> 
			</tr>
			<br />
			<tr>
				<td>Sufficiency of accomodation for reading<br></td>
				<td>'.$row['accomodation'].'</td> 
			</tr>
			<br />
			<tr>
				<td>Whether seperate rooms for science<br /> laboratory,library etc,are available<br></td>
				<td>'.$row['lab'].'</td> 
			</tr>
			<br />
			<tr>
				<td>No.of teachers subject-wise already<br />working with their name,qualification etc.<br>(if needed attach sheets) <br></td>
				<td>'.$row['teachers'].'</td> 
			</tr>
			<br />
			<tr>
				<td>Number of children enrolled in respective<br />classes relevent to purpose for which<br />assistance is being sought.(at least 3 years<br />data be given)<br></td>
				<td>'.$row['enrolled_child'].'</td> 
			</tr>
			<br />
   
    </table>
    </td>
    </tr>
    <br />
    <tr>
    	<td>12.</td>
    	<td>Action Photographs about the school</td>
        <td>:</td>
        <td>'.$row['photo'].'</td>
    </tr>
    <br />
    <tr>
    	<td>13.</td>
    	<td>Sucess stories/testimonials/award/recognitions<br /> and how the work of school has made difference to the individual,<br />family and for the community</td>
        <td>:</td>
        <td>'.$row['success_stories'].'</td>
        
    </tr>
    <br />
    <tr>
    	<td>14.</td>
    	<td>Community/client involement in the academic<br /> affairs pof school/educational institution</td>
        <td>:</td>
        <td>'.$row['client_involve'].'</td>
    </tr>
    <br />
    <tr>
    	<td>15.</td>
    	<td>future plans and sustainability</td>
        <td>:</td>
        <td>'.$row['future_plans'].'</td>
    </tr>
    <br />
    <tr>
    	<td>16.</td>
    	<td>Audited balance sheet,income and expenditure<br /> statement for the last three years of voluntary <br />agency and educational institution and school for<br /> which assistence is sought</td>
        <td>:</td>
        <td>'.$row['photo1'].'</td>
    </tr>
    <br />
    <tr>
    	<td>17.</td>
    	<td>Annual report if any of VA/educational institution or school</td>
        <td>:</td>
        <td>'.$row['photo2'].'</td>
    </tr>
    <br />
    <tr>
    	<td>18.</td>
    	<td>Inforamtion on existing funding sources with break <br />up grants,loans,and corpus received by VA and <br />educational institution/school for which assistance is being sought</td>
        <td>:</td>
        <td>'.$row['fund_info'].'</td>
    </tr>
    <br />
    <tr>
    	<td>19.</td>
    	<td>Whether the educational institution/school is<br /> receiving financial assistance for infrastructure<br /> development of any kinds from any other source;if<br /> so details thereof:</td>
        <td>:</td>
        <td>'.$row['fin_assistance'].'</td>
    </tr>
    <br />
    <tr>
    	<td>20.</td>
    	<td>Amount required for additional</td>
        <td>:</td>
        <td></td>
    </tr>
 

 
    <tr><td colspan="4">
        	<table border="1" align="center"   >
        		<tr>
        			<td>Item</td>
        			<td>Number</td>
        			<td>Amount required*</td>
        			<td>No. of children to be benefited</td>
        		
        		</tr>
        	<tr>
        		<td>classrooms</td>
        		<td>'.$row['num1'].'</td>
        		<td>'.$row['am1'].'</td>
        		<td>'.$row['bene1'].'</td>
        	</tr>
        	<tr>
        		<td>Science rooms</td>
        		<td>'.$row['num2'].'</td>
        		<td>'.$row['am2'].'</td>
        		<td>'.$row['bene2'].'</td>
        	</tr>
        	<tr>
        		<td>Computer lab rooms</td>
        		<td>'.$row['num3'].'</td>
        		<td>'.$row['am3'].'</td>
        		<td>'.$row['bene3'].'</td>
        	</tr>
        	<tr>
        		<td>Library rooms</td>
        		<td>'.$row['num4'].'</td>
        		<td>'.$row['am4'].'</td>
        		<td>'.$row['bene4'].'</td>
        	</tr>
        	<tr>
        		<td>Toilets(Girls)</td>
        		<td>'.$row['num5'].'</td>
        		<td>'.$row['am5'].'</td>
        		<td>'.$row['bene5'].'</td>
        	</tr>
        	<tr>
        		<td>Toilets(Boys)</td>
        		<td>'.$row['num6'].'</td>
        		<td>'.$row['am6'].'</td>
        		<td>'.$row['bene6'].'</td>
        	</tr>
        	<tr>
        		<td>Drinking water facilities</td>
	        	<td>'.$row['num7'].'</td>
	        	<td>'.$row['am7'].'</td>
	        	<td>'.$row['bene7'].'</td>
	        </tr>
        	<tr>
        		<td>Hostels for girls</td>
        		<td>'.$row['num8'].'</td>
	        	<td>'.$row['am8'].'</td>
	        	<td>'.$row['bene8'].'</td>
        	</tr>
        	<tr>
        		<td>Hostels for boys</td>
        		<td>'.$row['num9'].'</td>
	        	<td>'.$row['am9'].'</td>
	        	<td>'.$row['bene9'].'</td>
        	</tr>
        	<tr>
        		<td>Educational facilities like ramps/labs for children with special needs</td>
        		<td>'.$row['num10'].'</td>
	        	<td>'.$row['am10'].'</td>
	        	<td>'.$row['bene10'].'</td>
        	</tr>
        	<tr>
        		<td>Any other educational infrastucture requirement</td>
        		<td>'.$row['num11'].'</td>
	        	<td>'.$row['am11'].'</td>
	        	<td>'.$row['bene11'].'</td>
        	</tr>
        	<tr>
        		<td>Total</td>
        		<td>'.$row['num_tot'].'</td>
	        	<td>'.$row['am_tot'].'</td>
	        	<td>'.$row['bene_tot'].'</td>
        	</tr>
        	
        	
        	
        	</table></td>
        </tr>
        
    <br />
    <tr>
    		<td></td>
    		<td>Total in Words</td>
    		<td>:</td>
        	<td>';
			$html.=no_to_words($row['am_tot']);
        	$html .='</td>
        	</tr>
	<br />
    <tr>
    	<td>21.</td>
    	<td>Voluntary Organisation/Society s Share equal to<br /> 25% towards item 18 and commitment of<br /> voluntary Organisation/society to provide the same:</td>
        <td>:</td>
      <td>'.$row['org_share'].'</td>
    </tr>
    <tr>
    <br />
    	<td>22.</td>
    	<td>Source of investment of voluntary organisations/<br />society s share given against item 18</td>
        <td>:</td>
        <td>'.$row['source_investment'].'</td>
    </tr>
    <br />
    <tr>
    	<td>23.</td>
    	<td>Central Govt share equal to 75% reqiured against item 18</td>
        <td>:</td>
        <td>'.$row['govt_share'].'</td>
    </tr>
    
    <br />
    <tr>
    <td colspan="4">	
	It is certified that the information furnished abouve is true and I am personally liable for any misrepresentation or error.
	</td>    
    </tr>
   <br />
   
   <tr>
   <td colspan="4">Date:</td>
   </tr>
   
   <tr>
   <td colspan="4">Place:</td>
   </tr>
   
   <tr style="text-align:right">
   <td colspan="4">Signature of President/Chairman/Secretary</td>
   </tr>

   </table>		
</div>
</div>';

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');


$pdf->lastPage();	

// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('print.pdf', 'I');
function no_to_words($no)
{   
 $words = array('0'=> '' ,'1'=> 'one' ,'2'=> 'two' ,'3' => 'three','4' => 'four','5' => 'five','6' => 'six','7' => 'seven','8' => 'eight','9' => 'nine','10' => 'ten','11' => 'eleven','12' => 'twelve','13' => 'thirteen','14' => 'fouteen','15' => 'fifteen','16' => 'sixteen','17' => 'seventeen','18' => 'eighteen','19' => 'nineteen','20' => 'twenty','30' => 'thirty','40' => 'fourty','50' => 'fifty','60' => 'sixty','70' => 'seventy','80' => 'eighty','90' => 'ninty','100' => 'hundred &','1000' => 'thousand','100000' => 'lakh','10000000' => 'crore');
    if($no == 0)
        return ' ';
    else {
	$novalue='';
	$highno=$no;
	$remainno=0;
	$value=100;
	$value1=1000;       
            while($no>=100)    {
                if(($value <= $no) &&($no  < $value1))    {
                $novalue=$words["$value"];
                $highno = (int)($no/$value);
                $remainno = $no % $value;
                break;
                }
                $value= $value1;
                $value1 = $value * 100;
            }       
          if(array_key_exists("$highno",$words))
              return $words["$highno"]." ".$novalue." ".no_to_words($remainno);
          else {
             $unit=$highno%10;
             $ten =(int)($highno/10)*10;            
             return $words["$ten"]." ".$words["$unit"]." ".$novalue." ".no_to_words($remainno);
           }
    }
}
//============================================================+
// END OF FILE
//============================================================+
