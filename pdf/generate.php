<?php
//============================================================+
// File name   : example_006.php
// Begin       : 2008-03-04
// Last Update : 2013-05-14
//
// Description : Example 006 for TCPDF class
//               WriteHTML and RTL support
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: WriteHTML and RTL support
 * @author Nicola Asuni
 * @since 2008-03-04
 */

// Include the main TCPDF library (search for installation path).
require_once('tcpdf_include.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle('TCPDF Example 006');
$pdf->SetSubject('TCPDF Tutorial');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 006', PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('dejavusans', '', 10);

// add a page
$pdf->AddPage();

// writeHTML($html, $ln=true, $fill=false, $reseth=false, $cell=false, $align='')
// writeHTMLCell($w, $h, $x, $y, $html='', $border=0, $ln=0, $fill=0, $reseth=true, $align='', $autopadding=true)

// create some HTML content
$html = '<div>
<div float:left>
file no:'.$row['Fileno'].'<br />
Application id:'.$row['AppId'].'
</div>

<div style="font:bold;text-align:center;font-size:16px;">
SCHEME FOR DEVELOPMENT OF INFRASTRUCTURE IN <br />MINORITY EDUCATIONAL INSTITUTIONS OF<br /> ELEMENTARY/SECONDARY AND HIGHER SECONDARY LEVEL
<br />
<span style="font-weight:bold";text-align: center;>Application Form</span>
</div>
<div id="content">

<table style="">
	
	<tr valign="top">
		<td style="width:5%;">1.</td>
		<td style="width:40%;">Revenue district</td>
		<td style="width:1%;">:</td>
		<td style="width:43%;">'.$row['districts'].'</td>
	</tr>

    <tr>
    	<td>1.</td>
        <td>Name of Voluntary Organisation/Society <br /> managing the school(With complete address):</td>
        <td>:</td>
        <td>'.$row['orgname'].'</td>
    </tr>
    
	<tr valign="top">
		<td>3.</td>
		<td>School code</td>
		<td valign="center">:</td>
		<td>'.$row['code'].'</td>
	</tr>
	
    <br />
	<tr>
		<td>2.</td>
        <td>Name with address of the school/institution for financial assistance required</td>	
        <td>:</td>
        <td>'.$row['schoolname'].'</td>
    </tr>
    <br />
    <tr>
    	<td>3.</td>
    	<td>Objects and activities(give brief history of the organisation/society managing the school:)</td>
        <td>:</td>
        <td>'.$row['org_history'].'</td>
    </tr>
    <br />
    <tr>
    	<td>4.</td>
    	<td>Specific activities of the school for which financial <br /> assistance is sought under the scheme</td>
        <td>:</td>
        <td>'.$row['school_activities'].'</td>
    </tr>
    <br />
    <tr>
    	<td>5.</td>
    	<td>Whether registered under the Centra/State Board?<br />If Yes,Registration No.(A copy of	the registration certificate to be enclosed): </td>
        <td>:</td>
        <td>'.$row['registration'].'</td>
    </tr>
    <br />
    <tr>
    	<td>6.</td>
    	<td>Organisational structure,total staff.their roles and responsibilities,<br />staff turnover of educational <br />institute/school for which assistance is being<br />             sought and the voluntary organisation/society</td>
        <td>:</td>
        <td>'.$row['org_details'].'</td>
    </tr>
    <br />
    <tr  valign="top">
    	<td>7.</td>
    	<td>Governing Board/Managing Committe-number of <br />members,their role,meeting held and attendance,<br />their involvement in decision making of<br /> educational             institution/school and the voluntary<br /> organisation/society concerned(List of             members <br />may be enclosed)</td>
        <td>:</td>
        <td>'.$row['Govrning_board'].'</td>
    </tr>
    <br />
    <tr>
    	<td>8.</td>
    	<td>Name and address of bankers,auditors,legal<br /> advisors(including details of accounts)of voluntary organisation/society</td>
        <td>:</td>
        <td>'.$row['bankers'].'</td>
    </tr>
    <br />
    <tr>
    	<td>9.</td>
    	<td>Details of infra-structural facilities availible with<br /> educational institution/school for whom,<br /> assistance is being sought</td>
    	<td>:</td>
    	<td>'.$row['infra_details'].'</td>	
    </tr>
    <br />		
     <br />		
      <br />		 <br />		
    		<tr>
    		<td colspan="4">
    		
 			<table style="padding:0 60px;">
 			<tr>
	 			<td>Whether the building is rented or own?:<br></td>
				<td>'.$row['rented'].'</td> 
			</tr>
			<br />
			<tr>
				<td>No. of rooms availible for classes and<br /> Administrative purposes<br></td>
				<td>'.$row['classroom'].'</td> 
			</tr>
			<br />
			<tr>
				<td>Sufficiency of accomodation for reading<br></td>
				<td>'.$row['accomodation'].'</td> 
			</tr>
			<br />
			<tr>
				<td>Whether seperate rooms for science<br /> laboratory,library etc,are available<br></td>
				<td>'.$row['lab'].'</td> 
			</tr>
			<br />
			<tr>
				<td>No.of teachers subject-wise already<br />working with their name,qualification etc.<br>(if needed attach sheets) <br></td>
				<td>'.$row['teachers'].'</td> 
			</tr>
			<br />
			<tr>
				<td>Number of children enrolled in respective<br />classes relevent to purpose for which<br />assistance is being sought.(at least 3 years<br />data be given)<br></td>
				<td>'.$row['enrolled_child'].'</td> 
			</tr>
			<br />
   
    </table>
    </td>
    </tr>
    <br />
    <tr>
    	<td>10.</td>
    	<td>Action Photographs about the school</td>
        <td>:</td>
        <td>'.$row['photo'].'</td>
    </tr>
    <br />
    <tr>
    	<td>11.</td>
    	<td>Sucess stories/testimonials/award/recognitions<br /> and how the work of school has made difference to the individual,<br />family and for the community</td>
        <td>:</td>
        <td>'.$row['success_stories'].'</td>
        
    </tr>
    <br />
    <tr>
    	<td>12.</td>
    	<td>Community/client involement in the academic<br /> affairs pof school/educational institution</td>
        <td>:</td>
        <td>'.$row['client_involve'].'</td>
    </tr>
    <br />
    <tr>
    	<td>13.</td>
    	<td>future plans and sustainability</td>
        <td>:</td>
        <td>'.$row['future_plans'].'</td>
    </tr>
    <br />
    <tr>
    	<td>14.</td>
    	<td>Audited balance sheet,income and expenditure<br /> statement for the last three years of voluntary <br />agency and educational institution and school for<br /> which assistence is sought</td>
        <td>:</td>
        <td>'.$row['photo1'].'</td>
    </tr>
    <br />
    <tr>
    	<td>15.</td>
    	<td>Annual repeot if any of VA/educational institution or school</td>
        <td>:</td>
        <td>'.$row['photo2'].'</td>
    </tr>
    <br />
    <tr>
    	<td>16.</td>
    	<td>Inforamtion on existing funding sources with break <br />up grants,loans,and corpus received by VA and <br />educational institution/school for which assistance is being sought</td>
        <td>:</td>
        <td>'.$row['fund_info'].'</td>
    </tr>
    <br />
    <tr>
    	<td>17.</td>
    	<td>Whether the educational institution/school is<br /> receiving financial assistance for infrastructure<br /> development of any kinds from any other source;if<br /> so details thereof:</td>
        <td>:</td>
        <td>'.$row['fin_assistance'].'</td>
    </tr>
    <br />
    <tr>
    	<td>18.</td>
    	<td>Amount required for additional</td>
        <td>:</td>
        <td></td>
    </tr>
 

 
    <tr><td colspan="4">
        	<table border="1" align="center"   >
        		<tr>
        			<td>Item</td>
        			<td>Number</td>
        			<td>Amount required*</td>
        			<td>No. of children to be benefited</td>
        		
        		</tr>
        	<tr>
        		<td>classrooms</td>
        		<td>'.$row['num1'].'</td>
        		<td>'.$row['am1'].'</td>
        		<td>'.$row['bene1'].'</td>
        	</tr>
        	<tr>
        		<td>Science rooms</td>
        		<td>'.$row['num2'].'</td>
        		<td>'.$row['am2'].'</td>
        		<td>'.$row['bene2'].'</td>
        	</tr>
        	<tr>
        		<td>Computer lab rooms</td>
        		<td>'.$row['num3'].'</td>
        		<td>'.$row['am3'].'</td>
        		<td>'.$row['bene3'].'</td>
        	</tr>
        	<tr>
        		<td>Library rooms</td>
        		<td>'.$row['num4'].'</td>
        		<td>'.$row['am4'].'</td>
        		<td>'.$row['bene4'].'</td>
        	</tr>
        	<tr>
        		<td>Toilets(Girls)</td>
        		<td>'.$row['num5'].'</td>
        		<td>'.$row['am5'].'</td>
        		<td>'.$row['bene5'].'</td>
        	</tr>
        	<tr>
        		<td>Toilets(Boys)</td>
        		<td>'.$row['num6'].'</td>
        		<td>'.$row['am6'].'</td>
        		<td>'.$row['bene6'].'</td>
        	</tr>
        	<tr>
        		<td>Drinking water facilities</td>
	        	<td>'.$row['num7'].'</td>
	        	<td>'.$row['am7'].'</td>
	        	<td>'.$row['bene7'].'</td>
	        </tr>
        	<tr>
        		<td>Hostels for girls</td>
        		<td>'.$row['num8'].'</td>
	        	<td>'.$row['am8'].'</td>
	        	<td>'.$row['bene8'].'</td>
        	</tr>
        	<tr>
        		<td>Hostels for boys</td>
        		<td>'.$row['num9'].'</td>
	        	<td>'.$row['am9'].'</td>
	        	<td>'.$row['bene9'].'</td>
        	</tr>
        	<tr>
        		<td>Educational facilities like ramps/labs for children with special needs</td>
        		<td>'.$row['num10'].'</td>
	        	<td>'.$row['am10'].'</td>
	        	<td>'.$row['bene10'].'</td>
        	</tr>
        	<tr>
        		<td>'.$row['num11'].'</td>
	        	<td>'.$row['am11'].'</td>
	        	<td>'.$row['bene11'].'</td>
        	</tr>
        	<tr>
        		<td>Total</td>
        		<td>'.$row['num_tot'].'</td>
	        	<td>'.$row['am_tot'].'</td>
	        	<td>'.$row['bene_tot'].'</td>
        	</tr>
        	
        	
        	</table></td>
        </tr>
        
    <br />
    <tr>
    	<td>19.</td>
    	<td>Voluntary Organisation/Society s Share equal to<br /> 25% towards item 18 and commitment of<br /> voluntary Organisation/society to provide the same:</td>
        <td>:</td>
      <td>'.$row['org_share'].'</td>
    </tr>
    <tr>
    <br />
    	<td>20.</td>
    	<td>Source of investment of voluntary organisations/<br />society s share given against item 18</td>
        <td>:</td>
        <td>'.$row['source_investment'].'</td>
    </tr>
    <br />
    <tr>
    	<td>21.</td>
    	<td>Central Govt share equal to 75% reqiured against item 18</td>
        <td>:</td>
        <td>'.$row['govt_share'].'</td>
    </tr>
    <br />
    <tr>
    <td colspan="4">	
	It is certified that the information furnished abouve is true and I am personally liable for any misrepresentation or error.
	</td>    
    </tr>
   <br />
   
   <tr>
   <td colspan="4">Date:</td>
   </tr>
   
   <tr>
   <td colspan="4">Place:</td>
   </tr>
</table>		
</div>
</div>';

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');


// output some RTL HTML content
$html = '<div style="text-align:center">The words &#8220;<span dir="rtl">&#1502;&#1494;&#1500; [mazel] &#1496;&#1493;&#1489; [tov]</span>&#8221; mean &#8220;Congratulations!&#8221;</div>';
$pdf->writeHTML($html, true, false, true, false, '');

// test some inline CSS
$html = '<p>This is just an example of html code to demonstrate some supported CSS inline styles.
<span style="font-weight: bold;">bold text</span>
<span style="text-decoration: line-through;">line-trough</span>
<span style="text-decoration: underline line-through;">underline and line-trough</span>
<span style="color: rgb(0, 128, 64);">color</span>
<span style="background-color: rgb(255, 0, 0); color: rgb(255, 255, 255);">background color</span>
<span style="font-weight: bold;">bold</span>
<span style="font-size: xx-small;">xx-small</span>
<span style="font-size: x-small;">x-small</span>
<span style="font-size: small;">small</span>
<span style="font-size: medium;">medium</span>
<span style="font-size: large;">large</span>
<span style="font-size: x-large;">x-large</span>
<span style="font-size: xx-large;">xx-large</span>
</p>';

$pdf->writeHTML($html, true, false, true, false, '');

// reset pointer to the last page
$pdf->lastPage();

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Print a table

// add a page
$pdf->AddPage();

// create some HTML content
$subtable = '<table border="1" cellspacing="6" cellpadding="4"><tr><td>a</td><td>b</td></tr><tr><td>c</td><td>d</td></tr></table>';

$html = '<h2>HTML TABLE:</h2>
<table border="1" cellspacing="3" cellpadding="4">
	<tr>
		<th>#</th>
		<th align="right">RIGHT align</th>
		<th align="left">LEFT align</th>
		<th>4A</th>
	</tr>
	<tr>
		<td>1</td>
		<td bgcolor="#cccccc" align="center" colspan="2">A1 ex<i>amp</i>le <a href="http://www.tcpdf.org">link</a> column span. One two tree four five six seven eight nine ten.<br />line after br<br /><small>small text</small> normal <sub>subscript</sub> normal <sup>superscript</sup> normal  bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla<ol><li>first<ol><li>sublist</li><li>sublist</li></ol></li><li>second</li></ol><small color="#FF0000" bgcolor="#FFFF00">small small small small small small small small small small small small small small small small small small small small</small></td>
		<td>4B</td>
	</tr>
	<tr>
		<td>'.$subtable.'</td>
		<td bgcolor="#0000FF" color="yellow" align="center">A2 € &euro; &#8364; &amp; è &egrave;<br/>A2 € &euro; &#8364; &amp; è &egrave;</td>
		<td bgcolor="#FFFF00" align="left"><font color="#FF0000">Red</font> Yellow BG</td>
		<td>4C</td>
	</tr>
	<tr>
		<td>1A</td>
		<td rowspan="2" colspan="2" bgcolor="#FFFFCC">2AA<br />2AB<br />2AC</td>
		<td bgcolor="#FF0000">4D</td>
	</tr>
	<tr>
		<td>1B</td>
		<td>4E</td>
	</tr>
	<tr>
		<td>1C</td>
		<td>2C</td>
		<td>3C</td>
		<td>4F</td>
	</tr>
</table>';

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

// Print some HTML Cells

$html = '<span color="red">red</span> <span color="green">green</span> <span color="blue">blue</span><br /><span color="red">red</span> <span color="green">green</span> <span color="blue">blue</span>';

$pdf->SetFillColor(255,255,0);

$pdf->writeHTMLCell(0, 0, '', '', $html, 'LRTB', 1, 0, true, 'L', true);
$pdf->writeHTMLCell(0, 0, '', '', $html, 'LRTB', 1, 1, true, 'C', true);
$pdf->writeHTMLCell(0, 0, '', '', $html, 'LRTB', 1, 0, true, 'R', true);

// reset pointer to the last page
$pdf->lastPage();

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Print a table

// add a page
$pdf->AddPage();

// create some HTML content
$html = '<h1>Image alignments on HTML table</h1>
<table cellpadding="1" cellspacing="1" border="1" style="text-align:center;">
<tr><td><img src="images/logo_example.png" border="0" height="41" width="41" /></td></tr>
<tr style="text-align:left;"><td><img src="images/logo_example.png" border="0" height="41" width="41" align="top" /></td></tr>
<tr style="text-align:center;"><td><img src="images/logo_example.png" border="0" height="41" width="41" align="middle" /></td></tr>
<tr style="text-align:right;"><td><img src="images/logo_example.png" border="0" height="41" width="41" align="bottom" /></td></tr>
<tr><td style="text-align:left;"><img src="images/logo_example.png" border="0" height="41" width="41" align="top" /></td></tr>
<tr><td style="text-align:center;"><img src="images/logo_example.png" border="0" height="41" width="41" align="middle" /></td></tr>
<tr><td style="text-align:right;"><img src="images/logo_example.png" border="0" height="41" width="41" align="bottom" /></td></tr>
</table>';

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

// reset pointer to the last page
$pdf->lastPage();

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Print all HTML colors

// add a page
$pdf->AddPage();

$textcolors = '<h1>HTML Text Colors</h1>';
$bgcolors = '<hr /><h1>HTML Background Colors</h1>';

foreach(TCPDF_COLORS::$webcolor as $k => $v) {
	$textcolors .= '<span color="#'.$v.'">'.$v.'</span> ';
	$bgcolors .= '<span bgcolor="#'.$v.'" color="#333333">'.$v.'</span> ';
}

// output the HTML content
$pdf->writeHTML($textcolors, true, false, true, false, '');
$pdf->writeHTML($bgcolors, true, false, true, false, '');

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// Test word-wrap

// create some HTML content
$html = '<hr />
<h1>Various tests</h1>
<a href="#2">link to page 2</a><br />
<font face="courier"><b>thisisaverylongword</b></font> <font face="helvetica"><i>thisisanotherverylongword</i></font> <font face="times"><b>thisisaverylongword</b></font> thisisanotherverylongword <font face="times">thisisaverylongword</font> <font face="courier"><b>thisisaverylongword</b></font> <font face="helvetica"><i>thisisanotherverylongword</i></font> <font face="times"><b>thisisaverylongword</b></font> thisisanotherverylongword <font face="times">thisisaverylongword</font> <font face="courier"><b>thisisaverylongword</b></font> <font face="helvetica"><i>thisisanotherverylongword</i></font> <font face="times"><b>thisisaverylongword</b></font> thisisanotherverylongword <font face="times">thisisaverylongword</font> <font face="courier"><b>thisisaverylongword</b></font> <font face="helvetica"><i>thisisanotherverylongword</i></font> <font face="times"><b>thisisaverylongword</b></font> thisisanotherverylongword <font face="times">thisisaverylongword</font> <font face="courier"><b>thisisaverylongword</b></font> <font face="helvetica"><i>thisisanotherverylongword</i></font> <font face="times"><b>thisisaverylongword</b></font> thisisanotherverylongword <font face="times">thisisaverylongword</font>';

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

// Test fonts nesting
$html1 = 'Default <font face="courier">Courier <font face="helvetica">Helvetica <font face="times">Times <font face="dejavusans">dejavusans </font>Times </font>Helvetica </font>Courier </font>Default';
$html2 = '<small>small text</small> normal <small>small text</small> normal <sub>subscript</sub> normal <sup>superscript</sup> normal';
$html3 = '<font size="10" color="#ff7f50">The</font> <font size="10" color="#6495ed">quick</font> <font size="14" color="#dc143c">brown</font> <font size="18" color="#008000">fox</font> <font size="22"><a href="http://www.tcpdf.org">jumps</a></font> <font size="22" color="#a0522d">over</font> <font size="18" color="#da70d6">the</font> <font size="14" color="#9400d3">lazy</font> <font size="10" color="#4169el">dog</font>.';

$html = $html1.'<br />'.$html2.'<br />'.$html3.'<br />'.$html3.'<br />'.$html2;

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// test pre tag

// add a page
$pdf->AddPage();



// test custom bullet points for list

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('print.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
