<?php 
$page = $_SERVER['PHP_SELF'];
$sec = "5";
header("Refresh: $sec; url=$page");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link rel="stylesheet" type="text/css" href="style.css" />
<script src="JSCal2/js/jscal2.js"></script>
	<script src="JSCal2/js/lang/en.js"></script>
	<link rel="stylesheet" type="text/css" href="JSCal2/css/jscal2.css" />
	<link rel="stylesheet" type="text/css" href="JSCal2/css/border-radius.css" />
	<link rel="stylesheet" type="text/css" href="JSCal2/css/steel/steel.css" />

<script src="JSCal2/js/gen_validatorv31.js" type="text/javascript"></script>
<script>
function read(){
	document.getElementById("more").style.display="block";
}
	
</script>
</head>
<?php
error_reporting("E_NOTICE");
if(isset($_GET['ID']))
{
	$ID=$_GET['ID'];
	$APPID=$_GET['app'];
}
	else
{
	$complaint="";
	unset($_GET['ID']);
}
?>
<body>
	
	    					
<div id="container">
 
<div id="header">
	<div id="header_title">SCHEME FOR INFRASTRUCTURE DEVELOPMENT PRIVATE AIDED/UNAIDED<br />MINORITY INSTITUTES(IDMI)-(ELEMENTARY SECONDARY/SENIOR<br />SECONDARY SCHOOLS)</div>
  
  
</div>
<div id="menubar"><ul>
  	  	  <li><a href="index.php">Home</a></li>
          <li><a href="closed.php">New Application</a></li>
          <!-- <li><a href="edit.php">Edit Application</a></li> -->
          <!-- <li><a href="pdfgeneration.php">Print Application</a></li> -->
          <li><a href="search_application.php">Search Application status</a></li>
          <li><a href="login.php">Login</a></li>
        </ul>
</div>
<div id="body">

<div id="inner" style="text-align: center; font-size: 20px; font-family:"Arial Black" >
<span style="color:#FF0000; font-size: 19px;">		
<?php if(isset($_GET['ID'])) echo "Your File No is: ".$ID. " and Application ID is :" .$APPID; ?></span>
	<span style="float:right; color:#FF0000; margin-right: 30px;"><a href="Documents/Notification .pdf" style="text-decoration:none;color:#FF0000">Instructions</a></span>
<?php	if(isset($_GET['ID'])&&($_GET['app'])) echo '<script>alert("Your Application No is:'.$ID.' and Application Id is '.$APPID.'"); </script>';?>

</div>

<div id="middle1">
<div>
<span><marquee loop="loop"  behavior="alternate" direction="right" scrollamount="5" height="100"  style="font-size: 14px; font-style: oblique; color: #C90065" onmouseover="this.stop();" onmouseout="this.start();">Maximum financial Assistance is Rupees Fifity Lakh (Rs 5000000)<br />The last  date for IDMI updation has been extended to 15/03/2014</marquee></span>	
</div>
	

<span style="margin-left:101px;">Introduction:</span>
<div style="text-align: justify;margin-left:100px; width: 800px; font-size: 12px;"  >
<p>	National Policy on Education (NPE) has adopted the concept of a national system of
education, implying that all students irrespective of caste, creed, language or sex have
access to education of comparable quality. The Policy lays special emphasis on removal of
disparities and equalizing educational opportunities by attending to the specific needs of
those who have remained educationally backward so far.
</p>
<p>
	In pursuance of NPE stipulations, the scheme of Area Intensive Development for
ensuring an area intensive approach to education was launched in the IXth Plan. This
scheme was merged with Madrasas Modernisation Scheme during the Xth Plan with two
components of Modernisation and Infrastructure remaining separate. No financial
assistance for infrastructure development was envisaged for Madrasas to whom assistance
was restricted to providing honorarium for the teachers teaching modern subjects and
assistance under the infrastructure component was provided to Governmental and Non
Governmental educational institutions imparting modern education.
</p>
<p>
	Two components of infrastructure and modernization are sought to be demerged in
the XIth Plan in view of the fact that the target groups for these two components are
different. The component of Modernization of Madrasas has been recast and a separate
scheme of providing quality education in Madrasas drawn up. The infrastructure
component is proposed to be incorporated in this scheme – Infrastructure Development of
private aided/unaided Minority Institutes (IDMI) (elementary secondary/senior secondary
schools).
</p>
<div id="more" style="display: none">
<p>	OBJECTIVE:<br />
The scheme would facilitate education of minorities by augmenting and strengthening
school infrastructure in Minority Institutions (elementary/ secondary/senior secondary
schools) in order to expand facilities for formal education to children of minority
communities. The scheme will inter alia encourage educational facilities for girls, children
with special needs and those who are most deprived educationally amongst minorities.
</p>
<p>
COVERAGE:<br />
The scheme will cover the entire country. However, preference will be given to eligible
minority institutions (private aided/unaided elementary/ secondary/ senior secondary
schools) located in districts, blocks and towns having a minority population above 20%,
based on available census data.
</p>
<p>
	FINANCIAL PATTERN:<br />
6. An amount of Rs.125 crore is proposed for the scheme in the XIth Five Year Plan.
7. The scheme will fund infrastructure development of private aided/unaided minority
elementary/secondary/senior secondary schools to the extent of 75% and subject to a
maximum of Rs. 50 lakhs per school for:
Strengthening of educational infrastructure and physical facilities in the existing
(i)
elementary/secondary/senior secondary school including additional classrooms,
science / computer lab rooms, library rooms, toilets, drinking water facilities etc.
(ii) Hostel buildings for children in such category of schools, especially for girls.
(iii) Any other educational infrastructure not covered in (i) or (ii) above, but which in
view of the State/Central Grant in Aid Committee is justified for educational
advancement of the minority institution.
</p>
<p>
	ELIGIBILITY CONDITIONS:
8.
Voluntary
organizations/societies/trusts
running
institutes/schools
that
are
recognized by Central or State governments shall be eligible to apply for assistance under
the scheme.
9.
Only those voluntary agencies, which have been in existence for a minimum of three
years, would be considered for assistance under this scheme.
10. Voluntary organizations eligible under the scheme should:
have a proper constitution or Articles of Association;
 have a properly constituted managing body with its powers and duties clearly
defined in the constitution

be in a position to secure the involvement, on voluntary basis, of knowledgeable
persons for furtherance of their programmes; not be run for the profit of any
individual or a body of individuals;

not discriminate against any person or group of persons on the ground of language
or sex etc;

not function for the furtherance of the interests of any political party; nor in any
manner incite communal disharmony.
11. The institute/school for which assistance is being sought should have been functioning
for at least 3 years and have substantial enrolment of children from the minority
communities. The institution/school should not be a commercialized school charging high
fees.
12. The application of the voluntary organization will be addressed to the concerned
Secretary of the State/UT, who will be the Chairperson of Grant-in-Aid committee in the
State/UT
</p>
<p>
	IMPLEMENTATION AND MONITORING:
13. The scheme will be implemented through the State Government. All requests for
financial assistance entertained by the State Government in the prescribed application
form appended at Annexure will be considered on merit first by the State level Grant-In-Aid
committee. The State Government will draw up and notify criteria for prioritization of
applications under this scheme and give it wide publicity. The criterion should consider the
specific status of minorities in that State/UT and give priority to (i) direct educational
infrastructure requirements by which enrolment and retention of minority children in the
school/institution is likely to increase. (ii) encourage education of girls, children with
special needs and children from the more educationally backward minorities in the State.
(iii) the State level Grant-in-Aid Committee will recommend cases of voluntary
organizations and the specific schools/ institutions to be assisted, to the Central
Government in
order of priority.
A lower priority should be
accorded to
institutions/schools/minority organizations that have already received funds under the
erstwhile scheme of AIMMP, during the 10th Plan.
14. On receipt of proposals from the State governments the GIAC of the Central Govt. will
consider them on merit and recommend assistance.
15. A Grants-in-Aid Committee (GIAC) under the Chairmanship of Union Secretary, School
Education & Literacy would be constituted for the purpose, with Joint Secretary concerned
as Member-Secretary and FA(HRD) as member. It shall have representatives from Ministry
of Minority Affairs and State Governments concerned. The Ministry of HRD will nominate
two eminent educationists from amongst Minorities to the GIAC. The GIAC will examine
and approve the proposals of the State and UT Governments.
16. The financial assistance will be provided on a one time basis. A voluntary organization
or educational institution can receive benefit from the scheme only once in five years.
Funds will be released in two installments by GOI through the State Government.
The second installment will need to be claimed within one year of the first installment
during which period the 25% share of the minority institution, should have been
contributed to the infrastructure upgradation.
17. Voluntary Agencies receiving the assistance would be required to furnish audited
details of expenditure made in the prescribed proforma duly attested by a chartered
accountant/the competent authority.
18. The grant will be admissible to only those organizations/institutions which submit the
updated statement of accounts for all the grant-in-aid received by them under this scheme
in the past, duly certified by a chartered accountant.
19. Monitoring reports regarding the voluntary organizations receiving assistance will
indicate, total number of beneficiary students, amount received and utilized by the
institutions, shall be furnished to the Ministry of Human Resource Development
Government of India on an annual basis by the State Government, after obtaining the same
from voluntary organizations receiving assistance under the scheme.
20. The accounts/records of activities of the voluntary organization shall be available on
demand for inspection to Central/State Government/C&AG.
21. The Central & State Governments will widely publicize the scheme.
22. The performance of the scheme will be evaluated by the Central & State Governments
as appropriate but definitely once after completion of two years of its operation.
23. No claim will be entertained from any organizations/individual for consideration as a
permanent beneficiary since the scheme is to be implemented on purely voluntary basis
envisaging one time non-recurring financial assistance.

</p>
</div>
	<div>
		<span id="read" onclick="read();" >Read more.....</span>
		<!-- <a href="./Documents/IDMI-scheme.pdf" style="text-decoration:none";>Read more.....</a><p> -->
	</div>
	</div>

</div>
</div>
<div id="footerouter">
<div id="footer">© All Rights Reserved. EDP-cell, General Education Department, Govt of Kerala. </div>
</div>
</div>