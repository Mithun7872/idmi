<?php 
include_once 'submit.inc.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link rel="stylesheet" type="text/css" href="style.css" />
<script src="JSCal2/js/jscal2.js"></script>
	<script src="JSCal2/js/lang/en.js"></script>
	<link rel="stylesheet" type="text/css" href="JSCal2/css/jscal2.css" />
	<link rel="stylesheet" type="text/css" href="JSCal2/css/border-radius.css" />
	<link rel="stylesheet" type="text/css" href="JSCal2/css/steel/steel.css" />
	<link rel="stylesheet" type="text/javascript" href="JSCal2/js/jquery-1.7.2-min.js" />
		<script type="text/javascript" src="JSCal2/js/jquery-1.4.js"></script>

<script src="JSCal2/js/gen_validatorv31.js" type="text/javascript"></script>

<script>
	function back(){
		location.href='./index.php';
	}

		function show()
		{
			document.getElementById('regi').style.display="table-row";
		}
		function hide()
		{
			document.getElementById('regi').style.display="none";
		}
</script>

</head>
<body>
<div>
<div id="container">
<div id="header">
SCHEME FOR INFRASTRUCTURE DEVELOPMENT PRIVATE AIDED/UNAIDED<br />MINORITY INSTITUTES(IDMI)-(ELEMENTARY SECONDARY/SENIOR<br />SECONDARY SCHOOLS)
<br />&nbsp;
<br />

    	APPLICATION FORM

</div>
<div id="menubar"><ul>
     	<li><a href="index.php">Home</a></li>
       
         <li><a href="edit.php">Edit Application</a></li>
          
          
          
        </ul></div>


<div id="content" style="margin-top: 30px;">
	
<form name="for1" id="for1" method="post" action="" enctype="multipart/form-data">

 <input type="hidden" name="sylla_type" value="state"  />

<table align="center">
	<tr>
		<td>1.</td>
		<td>Revenue district</td>
		<td>:</td>
		<td>
			<select name="districts" id="districts" class="districts" style="width: 250px;">
															<option  selected="selected">--Revenue District--</option>
														<?php
														echo	$query="SELECT * FROM `rev_district_master`";
												 echo   		$result=mysql_query($query) or die("Selection query of District Master is Error ".mysql_error());
															$num = mysql_numrows($result);
															while($row=mysql_fetch_array($result))
												     		{
												     				 $ids=$row['rev_district_code'];
																	$data=$row['rev_district_name'];
																	echo '<option value="'.$ids.'">'.$data.'</option>';
												    		}
																?>
			</select>
		
		</td>
	
	</tr>

	<tr>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>	
	</tr>
		
    <tr valign="top">
    	<td valign="center">2.</td>
        <td>Name of Voluntary Organisation/Society managing <br /> the school(With complete address)</td>
      	<td>:</td>
    	<td><textarea name="orgname" id="orgname" style="width: 250px; height: 60px;"></textarea></td>
    </tr>
    
     <tr>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>	
	</tr>
    
	<tr valign="top">
		<td>3.</td>
		<td>School code</td>
		<td valign="center">:</td>
		<td><input type="text" name="code" id="code" class="code" size="12" onchange="getname();" style="width: 250px;" onkeyup="valid(this,'numbers')" onKeyPress="valid(this,'numbers')" /> </td>
	</tr>
	
	 <tr>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>	
	</tr>
	<!-- <tr class="hr"><td colspan="4"><hr /></td></hr> -->	
	<tr valign="top">
		
		<td>4.</td>
        <td>Name with address of the school/institution for <br /> financial assistance required</td>	
        <td valign="center">:</td>
        <td><textarea name="schoolname" id="schoolname" class="schoolname" style="width: 250px; height: 60px;"></textarea></td>
   </tr> 
    <tr valign="top">
    	<td>5.</td>
    	<td>Objects and activities(give brief history of the <br />/society managing the school:)</td>
        <td valign="center">:</td>
        <td><textarea name="org_history" id="org_history" style="width: 250px; height: 60px;"></textarea></td>
    </tr>
    <tr valign="top">
    	<td>6.</td>
    	<td>Specific activities of the school for which financial <br /> assistance is sought under the scheme</td>
        <td valign="center">:</td>
        <td><textarea name="school_activities" id="school_activities" style="width: 250px; height: 60px;"></textarea></td>
    </tr>
    <tr valign="top">
    	<td>7.</td>
    	<td>Whether registered under the Centra/State Board? </td>
        <td valign="center">:</td>
        <td><input type="radio"  name="registration" id="registration" value="Yes" onclick="show();"/>Yes
        	<input type="radio" name="registration"  id="registration" value="No"  onclick="hide();" />No
        </td>
    </tr>
    
    <tr valign="top" id="regi" style="display:none;">
    	<td style="text-indent:10px;">i)</td>
    	<td>If Yes,Registration No.</td>
    	<td>:</td>
    	<td ><textarea name="registrations" id="registrations" style="width: 250px; height: 60px;"></textarea></td>
    </tr>
    <tr valign="top">
    	<td>8.</td>
    	<td>Organisational structure,total staff.their roles and responsibilities,<br />staff turnover of educational <br />institute/school for which assistance is being<br />             sought and the voluntary organisation/society</td>
        <td valign="center">:</td>
        <td><textarea name="org_details" id="org_details"  style="width: 255px; height: 86px;"></textarea></td>
    </tr>
    <tr  valign="top">
    	<td>9.</td>
    	<td>Governing Board/Managing Committe-number of <br />members,their role,meeting held and attendance,<br />their involvement in decision making of<br /> educational             institution/school and the voluntary<br /> organisation/society concerned(List of             members <br />may be enclosed)</td>
        <td valign="center">:</td>
        <td><textarea name="Govrning_board" id="Govrning_board" style="width: 250px; height: 93px;"></textarea></td>
    </tr>
   
    <tr>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>	
	</tr>
    
    <tr>
    	<td>10.</td>
    	<td>Bank name</td>	
    	<td>:</td>
    	<!-- <td><input type="text" name="code" id="code" class="code" style="width: 250px;" value="<?php  echo $row['photo']; ?>"/></td> -->
    		<td>
    		<select name="banks" id="banks" class="banks" onchange="getbranch();">
															<option  selected="selected">--Banks--</option>
														<?php
															$query="SELECT * FROM `banks_in_kerala`";
												    		$result=mysql_query($query) or die("Selection query of District Master is Error ".mysql_error());
															$num = mysql_numrows($result);
															while($row=mysql_fetch_array($result))
												     		{
												     				$ids=$row['bank_name'];
																	$data=$row['bank_id'];
																	echo '<option value="'.$data.'">'.$ids.'</option>';
												    		}
																?>
														</select>
    		
    	</td>	
    </tr>
    
    <tr>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>	
	</tr>
    
    <tr>
    	<td>11.</td>
    	<td>Branch name</td>
    	<td>:</td>
    	<td>
    		<select style="width: 251px;" name="branch_name" id="branch_name" class="branch_name"  onchange="get_ifsc();">
				<option value="">--SELECT--</option>
		    </select>
    	</td>
    	
    </tr>
    
    <tr>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>	
	</tr>
    
     <tr>
    	<td>12.</td>
    	<td>IFSC Code</td>
    	<td>:</td>
    	<td>
    		<input type="text" name="ifsc" value="" class="ifsc" id="ifsc" style="width: 250px;" />
    	</td>
    	
    </tr>
    
    <tr>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>	
	</tr>
    
    <tr>
    	<td>13.</td>
    	<td>Account No:</td>
    	<td>:</td>
    	<td><input type="text" name="accno" value="" class="accno" id="accno" onkeyup="valid(this,'numbers')" onKeyPress="valid(this,'numbers')" style="width: 250px;"  /></td>
    </tr>
    
     <tr>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>	
	</tr>
    
    <tr valign="top">
    	<td>14.</td>
    	<td>Name and address of auditors,legal<br /> advisors(including details of accounts)of voluntary organisation/society</td>
        <td valign="center">:</td>
        <td><textarea name="bankers" id="bankers" style="width: 250px; height: 60px;"></textarea></td>
    </tr>
    <tr valign="top">
    	<td>15.</td>
    	<td>Details of infra-structural facilities availible with<br /> educational institution/school for whom,<br /> assistance is being sought</td>
    	<td valign="center">:</td>
    	<td><textarea name="infra_details" id="infra_details" style="width: 250px; height: 60px;"></textarea></td>	
    </tr>		
    		<tr>
    			<td></td>
    			<td>
    			<table width="143%">
    				<tr valign="top">
    					<td style="margin-left:60px;">a.</td>
    					<td width="700">Whether the building is rented or own?</td>
    					<td width="93" valign="center">:</td>
    					<td width="184"><textarea name="rented" id="rented"  style="width: 170px; height: 63px;"></textarea></td>
    				</tr>
    				<tr valign="top">
    					<td>b.</td>
    					<td>No. of rooms availible for classes and<br /> Administrative purposes</td>
    					<td valign="center">:</td>
    					<td><textarea name="classroom" id="classroom"   style="width: 170px; height: 63px;"></textarea></td>
    				</tr>
    				<tr valign="top">
    					<td>c.</td>
    					<td>Sufficiency of accomodation for reading</td>
    					<td valign="center">:</td>
    					<td><textarea name="accomodation" id="accomodation"   style="width: 170px; height: 63px;"></textarea></td>
    				</tr>
    				<tr valign="top">
    					<td>d.</td>
    					<td>Whether seperate rooms for science laboratory,library etc,are available. </td>
    					<td valign="center">:</td>
    					<td><textarea name="lab" id="lab"   style="width: 170px; height: 63px;"></textarea></td>
    				</tr>
    				<tr valign="top">
    					<td>e.</td>
    					<td>No.of teachers subject-wise already<br />working with their name,qualification etc.<br>(if needed attach sheets)  </td>
    					<td valign="center">:</td>
    					<td><textarea name="teachers" id="teachers"   style="width: 170px; height: 63px;"></textarea></td>
    				</tr>
    				<tr valign="top">
    					<td>f.</td>
    					<td>Number of children enrolled in respective<br />classes relevent to purpose for which<br />assistance is being sought.(at least 3 years<br />data be given) </td>
    					<td valign="center">:</td>
    					<td><textarea name="enrolled_child" id="enrolled_child"   style="width: 170px; height: 63px;"></textarea><br />	</td>
    				</tr>
    			</table>
 				</td>   	
    		</tr>
    		
    		 <tr>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>	
	</tr>
    		
    <tr valign="top">
    	<td>16.</td>
    	<td>Action Photographs about the school</td>
        <td valign="center">:</td>
        <td><input type="text" id="photo" name="photo" style="width: 250px; " /></td>
    </tr>
    <tr valign="top">
    	<td>17.</td>
    	<td>Sucess stories/testimonials/award/recognitions<br /> and how the work of school has made difference to the individual,<br />family and for the community</td>
        <td valign="center">:</td>
        <td><textarea name="success_stories" id="success_stories"  style="width: 250px; height: 60px;"></textarea></td>
        
    </tr>
    <tr valign="top">
    	<td>18.</td>
    	<td>Community/client involement in the academic<br /> affairs pof school/educational institution</td>
        <td valign="center">:</td>
        <td><textarea name="client_involve" id="client_involve"  style="width: 250px; height: 60px;"></textarea></td>
    </tr>
    <tr valign="top">
    	<td>19.</td>
    	<td>future plans and sustainability</td>
        <td valign="center">:</td>
        <td><textarea name="future_plans" id="future_plans"  style="width: 250px; height: 60px;"></textarea></td>
    </tr>
    <tr valign="top">
    	<td>20.</td>
    	<td>Audited balance sheet,income and expenditure<br /> statement for the last three years of voluntary <br />agency and educational institution and school for<br /> which assistence is sought</td>
        <td valign="center">:</td>
        <td><textarea name="photo1" id="photo1"  style="width: 250px; height: 60px;"></textarea></td>
    </tr>
     <tr>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>	
	</tr>
    
    <tr valign="top">
    	<td>21.</td>
    	<td>Annual repeot if any of VA/educational institution or school</td>
        <td valign="center">:</td>
        <td><textarea name="photo2" id="photo2" style="width: 250px; height: 60px;"></textarea></td>
    </tr>
    <tr valign="top">
    	<td>22.</td>
    	<td>Inforamtion on existing funding sources with break <br />up grants,loans,and corpus received by VA and <br />educational institution/school for which assistance is being sought</td>
        <td valign="center">:</td>
        <td><textarea name="fund_info" id="fund_info"  style="width: 250px; height: 60px;"></textarea></td>
    </tr>
    <tr valign="top">
    	<td>23.</td>
    	<td>Whether the educational institution/school is<br /> receiving financial assistance for infrastructure<br /> development of any kinds from any other source;if<br /> so details thereof:</td>
        <td valign="center">:</td>
        <td><input type="radio"  name="fin_assistance" id="fin_assistance" value="Yes" />Yes
        	<input type="radio" name="fin_assistance" id="fin_assistance" value="No" />No</td>
    </tr>
    <tr valign="top">
    	<td>24.</td>
    	<td>Amount required for additional</td>
        <td valign="center">:</td>
        <td></td>
    </tr>
    <tr><td colspan="4">
        	<table border="1" align="center"   >
        		<tr>
        			<td>Item</td>
        			<td>Number</td>
        			<td>Amount required*</td>
        			<td>No. of children to be benefited</td>
        		
        		</tr>
        	<tr>
        		<td>classrooms</td>
        		<td><input type="text" name="num1"  id="num1" size="15" onkeyup="valid(this,'numbers')" onKeyPress="valid(this,'numbers')" /></td>
        		<td><input type="text" name="am1" class="am1" id="am1" size="15" onblur="findTotal1" onkeyup="valid(this,'numbers')" onKeyPress="valid(this,'numbers')" onchange="calc()"/></td>
        		<td><input type="text" name="bene1" id="bene1" size="20" onkeyup="valid(this,'numbers')" onKeyPress="valid(this,'numbers')"/></td>
        	</tr>
        	<tr>
        		<td>Science rooms</td>
        		<td><input type="text" name="num2" id="num2" size="15"/ onkeyup="valid(this,'numbers')" onKeyPress="valid(this,'numbers')"></td>
        		<td><input type="text" name="am2" id="am2" class="am2" size="15" onblur="findTotal1" onkeyup="valid(this,'numbers')" onKeyPress="valid(this,'numbers')" onchange="calc()"/></td>
        		<td><input type="text" name="bene2" id="bene2" size="20" onkeyup="valid(this,'numbers')" onKeyPress="valid(this,'numbers')"/></td>
        	</tr>
        	<tr>
        		<td>Computer lab rooms</td>
        		<td><input type="text" name="num3" id="num3" size="15" onkeyup="valid(this,'numbers')" onKeyPress="valid(this,'numbers')"/></td>
        		<td><input type="text" name="am3" id="am3" class="am3" size="15" onblur="findTotal1" onkeyup="valid(this,'numbers')" onKeyPress="valid(this,'numbers')" onchange="calc()"/></td>
        		<td><input type="text" name="bene3" id="bene3" size="20" onkeyup="valid(this,'numbers')" onKeyPress="valid(this,'numbers')"/></td>
        	</tr>
        	<tr>
        		<td>Library rooms</td>
        		<td><input type="text" name="num4" id="num4" size="15" onkeyup="valid(this,'numbers')" onKeyPress="valid(this,'numbers')"/></td>
        		<td><input type="text" name="am4" id="am4" class="am4" size="15" onblur="findTotal1" onkeyup="valid(this,'numbers')" onKeyPress="valid(this,'numbers')" onchange="calc()"/></td>
        		<td><input type="text" name="bene4" id="bene4" size="20" onkeyup="valid(this,'numbers')" onKeyPress="valid(this,'numbers')"/></td>
        	</tr>
        	<tr>
        		<td>Toilets(Girls)</td>
        		<td><input type="text" name="num5" id="num5" size="15" onkeyup="valid(this,'numbers')" onKeyPress="valid(this,'numbers')"/></td>
        		<td><input type="text" name="am5" id="am5" class="am5" size="15" onblur="findTotal1" onkeyup="valid(this,'numbers')" onKeyPress="valid(this,'numbers')" onchange="calc()"/></td>
        		<td><input type="text" name="bene5" id="bene5" size="20" onkeyup="valid(this,'numbers')" onKeyPress="valid(this,'numbers')"/></td>
        	</tr>
        	<tr>
        		<td>Toilets(Boys)</td>
        	<td><input type="text" name="num6" id="num6" size="15" onkeyup="valid(this,'numbers')" onKeyPress="valid(this,'numbers')"/></td>
        		<td><input type="text" name="am6" id="am6" class="am6" size="15" onblur="findTotal1" onkeyup="valid(this,'numbers')" onKeyPress="valid(this,'numbers')" onchange="calc()"/></td>
        		<td><input type="text" name="bene6" id="bene6" size="20" onkeyup="valid(this,'numbers')" onKeyPress="valid(this,'numbers')"/></td>
        	</tr>
        	<tr>
        		<td>Drinking water facilities</td>
        	<td><input type="text" name="num7" id="num7" size="15" onkeyup="valid(this,'numbers')" onKeyPress="valid(this,'numbers')"/></td>
        		<td><input type="text" name="am7" id="am7" class="am7" size="15" onblur="findTotal1" onkeyup="valid(this,'numbers')" onKeyPress="valid(this,'numbers')" onchange="calc()"/></td>
        		<td><input type="text" name="bene7" id="bene7" size="20" onkeyup="valid(this,'numbers')" onKeyPress="valid(this,'numbers')"/></td>
        	</tr>
        	<tr>
        		<td>Hostels for girls</td>
        		<td><input type="text" name="num8" id="num8" size="15" onkeyup="valid(this,'numbers')" onKeyPress="valid(this,'numbers')"/></td>
        		<td><input type="text" name="am8" id="am8" class="am8" size="15" onblur="findTotal1" onkeyup="valid(this,'numbers')" onKeyPress="valid(this,'numbers')" onchange="calc()"/></td>
        		<td><input type="text" name="bene8" id="bene8" size="20" onkeyup="valid(this,'numbers')" onKeyPress="valid(this,'numbers')"/></td>
        	</tr>
        	<tr>
        		<td>Hostels for boys</td>
        		<td><input type="text" name="num9" id="num9" size="15" onkeyup="valid(this,'numbers')" onKeyPress="valid(this,'numbers')"/></td>
        		<td><input type="text" name="am9" id="am9" class="am9" size="15" onblur="findTotal1" onkeyup="valid(this,'numbers')" onKeyPress="valid(this,'numbers')" onchange="calc()"/></td>
        		<td><input type="text" name="bene9" id="bene9" size="20" onkeyup="valid(this,'numbers')" onKeyPress="valid(this,'numbers')"/></td>
        	</tr>
        	
        	<tr>
        		<td>Educational facilities like ramps/labs for children with special needs</td>
        		<td><input type="text" name="num10" id="num10" size="15" onkeyup="valid(this,'numbers')" onKeyPress="valid(this,'numbers')"/></td>
        		<td><input type="text" name="am10" id="am10" class="am10" size="15" onblur="findTotal1" onkeyup="valid(this,'numbers')" onKeyPress="valid(this,'numbers')" onchange="calc()"/></td>
        		<td><input type="text" name="bene10" id="bene10" size="20" onkeyup="valid(this,'numbers')" onKeyPress="valid(this,'numbers')"/></td>
        	</tr>
        	
        	<tr>
        		<td>any other educational infrastucture requirement</td>
        		<td><input type="text" name="num11" id="num11" size="15" onkeyup="valid(this,'numbers')" onKeyPress="valid(this,'numbers')"/></td>
        		<td><input type="text" name="am11" id="am11" class="am11" size="15" onblur="findTotal1" onkeyup="valid(this,'numbers')" onKeyPress="valid(this,'numbers')" onchange="calc()"/></td>
        		<td><input type="text" name="bene11" id="bene11" size="20" onkeyup="valid(this,'numbers')" onKeyPress="valid(this,'numbers')"/></td>
        	</tr>
        	
        	<tr>
        		<td>Total</td>
        	<td><input type="text" name="num_tot" class="num_tot" id="num_tot" size="15" onkeyup="valid(this,'numbers')" onKeyPress="valid(this,'numbers')"/></td>
        		<td><input style="font-weight: bold;" type="text" name="am_tot" readonly="readonly" class="am_tot" id="am_tot"  size="15" onkeyup="valid(this,'numbers')" onKeyPress="valid(this,'numbers')"/></td>
        		<td><b><input type="text" name="bene_tot" class="bene_tot" id="bene_tot"  size="20" onkeyup="valid(this,'numbers')" onKeyPress="valid(this,'numbers')"/></b></td>
        	</tr>
        	<tr>
        		<td colspan="4"><?php ?></td>
        		
        	</tr>
        	
        	
        	</table></td>
        </tr>
        
    
    <tr valign="top">
    	<td>25.</td>
    	<td>Voluntary Organisation/Society's Share equal to<br /> 25% towards item 18 and commitment of<br /> voluntary Organisation/society to provide the same:</td>
        <td valign="center">:</td>
        <td><textarea style="font-weight: bold;" name="org_share" readonly="readonly" class="org_share" id="org_share"  style="width: 250px; height: 60px;"></textarea></td>
    </tr>
    <tr valign="top">
    	<td>26.</td>
    	<td>Source of investment of voluntary organisations/<br />society's share given against item 18</td>
        <td valign="center">:</td>
        <td><textarea name="source_investment" id=""></textarea></td>
    </tr>
    <tr valign="top">
    	<td>27.</td>
    	<td>Central Govt share equal to 75% reqiured against item 18</td>
        <td valign="center">:</td>
        <td><textarea style="font-weight: bold;" name="govt_share" readonly="readonly" class="govt_share" id="govt_share"  style="width: 250px; height: 60px;"></textarea></td>
    </tr>
    
     <tr>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>	
	</tr>
    
    <tr>
    	<td colspan="4" align="center"><u>School Basic Details</u></td>
    </tr>
     <tr>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>	
	</tr>
    
     <tr valign="top">
    	<td>28.</td>
    	<td>Community of management</td>
        <td valign="center">:</td>
        <td><textarea name="community_mngmt" id="community_mngmt"  style="width: 250px; height: 60px;"></textarea></td>
    </tr>
     <tr>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>	
	</tr>
    
    <tr valign="top">
    	<td>29.</td>
    	<td>Specify Category(Aided,Unaided)</td>
        <td valign="center">:</td>
        <td><textarea id="Category" name="Category" class="Category"></textarea></td>
    </tr>
    
    <tr>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>	
	</tr>
    
    <tr valign="top">
    	<td>30.</td>
    	<td>Category of Agency(corporate,charitable Trust Etc.)</td>
        <td valign="center">:</td>
        <td><textarea name="Agency_cate" id="Agency_cate"  style="width: 250px; height: 60px;"></textarea></td>
    </tr>
      <tr>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>	
	</tr>
    
    
    <tr valign="top">
    	<td>31.</td>
    	<td>Whether Higher secondary,Secondary or Primary</td>
        <td valign="center">:</td>
        <!-- <td><textarea name="school_type" id="school_type"  style="width: 250px; height: 60px;"></textarea></td> -->
        <td>
        	<select name="school_type" id="school_type" style="width: 250px;" >
        		<option>--Select--</option>
        		<option>Higher Secondary</option>
        		<option>Secondary</option>
        		<option>Primary</option>
        	</select>
        </td>
    </tr>
    
      <tr>
	<td>&nbsp;</td>
	<td>&nbsp;</td> 
	<td>&nbsp;</td>
	<td>&nbsp;</td>	
	</tr>
    
     <tr>
    	<td>32.</td>
    	<td>Specify No.& date of Recongition</td>
        <td valign="center">:</td>
        <td><textarea name="Recongition_no" id="Recongition_no"  style="width: 250px; height: 60px;"></textarea></td>
     </tr>
    <tr valign="top">
    	<td >33.</td>
    	<td>Specify the Registration No.& Date of Minority Status Certificate Obtained From the National Minority Commision</td>
        <td valign="center">:</td>
        <td><textarea name="Minority_Commision_date" id="Minority_Commision_date"  style="width: 250px; height: 60px;"></textarea></td>
    </tr>
    
    <tr valign="top">
    	<td>34.</td>
    	<td>Whether the agency has three years of existence</td>
        <td valign="center">:</td>
        <td><input type="radio" name="agency_exp" id="agency_exp" value="Yes" />Yes
        	<input type="radio" name="agency_exp" id="agency_exp" value="No" />No
        </td>
    </tr>
    
    <tr>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>	
	</tr>
    
     <tr valign="top">
    	<td>35.</td>		
    	<td>Whether the school has three years of existence</td>
        <td valign="center">:</td>
        <td><input type="radio" name="school_exp" id="school_exp" value="Yes"  />Yes
        <input type="radio" name="school_exp" id="school_exp" value="No" />No</td>
    </tr>
    
    <tr>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>	
	</tr>
    
    <tr valign="top">
    	<td>36.</td>
    	<td>Whether Performance report enclosed with application</td>
        <td>:</td>
        <td><input type="radio" name="Performance_report" id="Performance_report" value="yes" />Yes
        <input type="radio" name="Performance_report" id="Performance_report" value="No"/>No</td>
        <!-- <td><textarea name="Performance_report" id="Performance_report" ></textarea></td> -->
    </tr>
    
     <tr>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>	
	</tr>
	
    <tr valign="top"> 
    	<td>37.</td>
    	<td>No.of students in the school(for 2013-14)</td>
        <td>:</td>
        <td><textarea name="school_studno" id="school_studno"  style="width: 250px; height: 60px;"></textarea></td>
    </tr>
   
    <tr>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>	
	</tr>
   
    <tr valign="top">
    	<td>38.</td>
    	<td>Source of commitment of the agencies to provide the amount</td>
        <td>:</td>
        <td><textarea name="Source_commitment" id="Source_commitment" style="width: 250px; height: 60px;" ></textarea></td>
    </tr>
    <tr valign="top">
    	<td>39.</td>
    	<td>Ground for grant</td>
        <td>:</td>
        <td><textarea name="Ground_grant" id="Ground_grant"  style="width: 250px; height: 60px;" ></textarea></td>
    </tr>
    <tr valign="top">
    	<td>40.</td>	
    	<td>Details of Proposed work to be done by using financial assistance.Write each item</td>
        <td>:</td>
        <td><textarea name="Details_Proposal" id="Details_Proposal"  style="width: 250px; height: 60px;" ></textarea></td>
    </tr>
   	<tr valign="top">
   		<td></td>
   		<td></td>
   		<td><input type="submit" name="submit" id="submit" value="submit"  /></td>
   		<td></td>
   	</tr>
</table>
</form>		
</div>
</div>
<div id="footerouter1">
<div id="footer">© All Rights Reserved. IT@School, General Education Department, Govt of Kerala. </div>
 </div> -->	 -->

<script>
// 
							 var frmvalidator = new Validator("for1");
							 frmvalidator.addValidation("districts","req","Please enter District");
							 frmvalidator.addValidation("orgname","req","Please Enter Organisation");
							 frmvalidator.addValidation("code","req","Please enter a valid School code");
							 frmvalidator.addValidation("schoolname","req","Enter School name");
						     frmvalidator.addValidation("org_history","req","Enter Organisation History");
						     frmvalidator.addValidation("school_activities","req","Enter the school activities");
						     frmvalidator.addValidation("registrations","req","Enter the Registration details");
						     frmvalidator.addValidation("org_details","req","Enter the Organisational details");
							 frmvalidator.addValidation("Govrning_board","req","Enter the Govrning_board details");
							 frmvalidator.addValidation("accno","req","Enter the Account no");
							 frmvalidator.addValidation("bankers","req","Enter the legal bankers details");	
							 frmvalidator.addValidation("infra_details","req","Enter the Infrastructure Details");
							 frmvalidator.addValidation("success_stories","req","Enter the Success Stories");
							 frmvalidator.addValidation("Details_Proposal","req","Enter the Organisational details");
							 frmvalidator.addValidation("fin_assistance","req","Enter the Financial assistance details");
							 frmvalidator.addValidation("agency_exp","req","Enter the Agency Experience");
							 frmvalidator.addValidation("school_exp","req","Enter the School Experience");
							 frmvalidator.addValidation("Performance_report_details","req","Enter the Organisational details");
function textarea(){
	
document.getElementById("regi").style.display="block";
}
	
	function getname()
{ 
	
	var value=$(".code").val();
	$.post("ajax/schooname.php",{cval:value,flag:2},
	
	function(data)
	{
		
		$(".schoolname").html(data);});


	var value=$(".code").val();
	$.post("ajax/schooname.php",{cval1:value,flag:1},
	
	function(data)
	{
		// alert(data);
		$(".Category").html(data);});
		
		
		var value=$(".code").val();
		var value1=$(".districts").val();
	$.post("ajax/schooname.php",{cval2:value,dist:value1,flag:3},
	
	function(data)
	{
		if(data==""){
			$("#err").hide();
	 // alert(data);
		// alert ($("#code").after("This School code doesnot belongs to "+value1+""));
		 alert("This School code doesnot belongs to this revenue district");
		$("#code").val('');
	}
	else
		$("#err").hide();
		});

}
 
 function getbranch(){
 	var value=$(".banks").val();
 	//alert(value);
 	//get_ifsc(value);
 	$.post("ajax/bankbranch.php",{cval:value,flag:2},
 	function(data)
 	{
 		//alert(data);
 		$(".branch_name").html(data);});
 }
 function get_ifsc(){
 	var value=$(".branch_name").val();
 	 	//var value=$(".banks").val();
 	//alert(value);
 	//get_ifsc(value);
 	$.post("ajax/bankbranch.php",{cvalue:value,flag:1},
 	function(data1)
 	{
 		// alert(data1);
 		$(".ifsc").val(data1);});
 }
 	// function getbranch()
// { 
	// alert("dfv");
	// var value=$(".banks").val();
	// $.post("ajax/bankbranch.php",{cval:value},
// 	
	// function(data)
	// {
		// alert(data);
		// $(".schoolname").html(data);});
// }
// alert("sdgfsdsd");
	

 function calc()
	
      { 
      	 am1=$(".am1").val();if(!am1) am1=0; 
         am2=$(".am2").val(); if(!am2) am2=0; 
         am3=$(".am3").val(); if(!am3) am3=0; 
         am4=$(".am4").val(); if(!am4) am4=0; 
         am5=$(".am5").val(); if(!am5) am5=0; 
         am6=$(".am6").val(); if(!am6) am6=0; 
         am7=$(".am7").val(); if(!am7) am7=0; 
         am8=$(".am8").val(); if(!am8) am8=0; 
         am9=$(".am9").val(); if(!am9) am9=0; 
         am10=$(".am10").val(); if(!am10) am10=0; 
       	  am11=$(".am11").val(); if(!am11) am11=0; 
         
         tot=parseInt(am1)+parseInt(am2)+parseInt(am3)+parseInt(am4)+parseInt(am5)+parseInt(am6)+parseInt(am7)+parseInt(am8)+parseInt(am9)+parseInt(am10)+parseInt(am11);
		
			$(".am_tot").val(tot);	
		

	
		 
		 twentyfive=$(".am_tot").val();
		 percent=parseInt(twentyfive * 25)/100;	
    	$(".org_share").val(Math.round(percent));
    	
    
    	seventyfive=parseInt(twentyfive * 75)/100;
    	if(seventyfive<5000000){
		
		$(".govt_share").val(Math.round(seventyfive));	
		}
		else{
    	$(".govt_share").val(5000000);
    }
      }
  
  
    
</script>
<script type="text/javascript">
function valid(o,w){
o.value = o.value.replace(valid.r[w],'');
}
valid.r={
'special':/[\W]/g,
'quotes':/['\''&'\"']/g,
'numbers':/[^\d]/g
}
?>			

</script>
</body>
</html>