<?php
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2013 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2013 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.7.9, 2013-06-02
 */

/** Error reporting */
error_reporting(E_ERROR);
include '../../library/dbconnect.php';
 
$query4="SELECT * FROM input_details ";
$result4=mysql_query($query4)or die(mysql_error());




ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/London');

if (PHP_SAPI == 'cli')
	die('This example should only be run from a Web Browser');

/** Include PHPExcel */
require_once '../Classes/PHPExcel.php';


// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

$objPHPExcel->setActiveSheetIndex();
	 $objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A3);

$objPHPExcel->getDefaultStyle()->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getDefaultStyle()->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getDefaultStyle()->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getDefaultStyle()->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
// $objPHPExcel->$sheet2()->mergeCells("A1:A5");
// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
							 ->setLastModifiedBy("Maarten Balliauw")
							 ->setTitle("Office 2007 XLSX Test Document")
							 ->setSubject("Office 2007 XLSX Test Document")
							 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("Test result file");

 
// $sheet = $phpExcel->getActiveSheet();
// $sheet = $phpExcel->getActiveSheetIndex()
//Heading
$objPHPExcel->setActiveSheetIndex(0)
->setCellValue("A1", "SCHEME FOR INFRASTRUCTURE DEVELOPMENT FOR MINORITY INSTITUTES (IDMI) IN KERALA 2014-15")
->setCellValue("A2", "PROPOSAL FOR THE YEAR 2014-15(KERALA)")
->setCellValue("A3", "PART-I");


$objPHPExcel->getActiveSheet()->getStyle('A3:T3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('A1:T1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('A2:T2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
// $objPHPExcel->getActiveSheet()->getColumnDimension("D4")->setWidth(50);
// $objPHPExcel->getActiveSheet()->getRowDimension("2")->setRowHeight(100);
$objPHPExcel->getActiveSheet()->getStyle("D2")->getAlignment()->setTextRotation(90);

$objPHPExcel->getActiveSheet()->getStyle("A1:T3")->applyFromArray(array("font" => array( "bold" => true)));
// $objPHPExcel->getActiveSheet()->getColumnDimension("A")->setAutoSize(true);


$objPHPExcel->getActiveSheet()->mergeCells("A1:T1");
$objPHPExcel->getActiveSheet()->mergeCells("A2:T2");
$objPHPExcel->getActiveSheet()->mergeCells("A3:T3");
$objPHPExcel->getActiveSheet()->setTitle('IDMI');


$objPHPExcel->setActiveSheetIndex(0)
->setCellValue("A4", "Application No.")
->setCellValue("B4", "District")
->setCellValue("C4", "Name & Address of the school/institution")
->setCellValue("D4", "Name & Address of the Agency")
->setCellValue("E4", "Community of management")
->setCellValue("F4", "Specify Category(Aided,Unaided)")
->setCellValue("G4", "Category of Agency(corporate,charitable Trust Etc.)")
->setCellValue("H4", "Whether Higher secondary,Secondary or Primary")
->setCellValue("I4", "Specify No.& date of Recongition")
->setCellValue("J4", "Specify the Registration No.& Date of Minority Status Certificate Obtained From the National Minority Commision")
->setCellValue("K4", "Whether the agency has three years of existence")
->setCellValue("L4", "Whetehr the school has three Years of Existence")
->setCellValue("M4", "Whether Performance report enclosed with application")
->setCellValue("N4", "No.of students in the school(for 2014-15)")
->setCellValue("O4", "Estimated amount(Item No.18 of the appln.)")
->setCellValue("P4", "75% of estimated amount against item No 18 or 50 Lakhs Whichever is less")
->setCellValue("Q4", "Amount Committed by the agency(Estimated amount minus Central grant)")
->setCellValue("R4", "Source of commitment of the agencies to provide the amount")
->setCellValue("S4", "Ground for grant")
->setCellValue("T4", "Details of Proposed work to be done by using financial assistance.Write each item");
// $objPHPExcel->getActiveSheet()->getRowDimension('A4')->setRowHeight(100);
$objPHPExcel->getActiveSheet()->getRowDimension('4')->setRowHeight(400);
$objPHPExcel->getActiveSheet()->getColumnDimension("A")->setWidth(2.24);
$objPHPExcel->getActiveSheet()->getColumnDimension("B")->setWidth(2.24);
$objPHPExcel->getActiveSheet()->getColumnDimension("C")->setWidth(9.64);
$objPHPExcel->getActiveSheet()->getColumnDimension("D")->setWidth(10.64);
$objPHPExcel->getActiveSheet()->getColumnDimension("E")->setWidth(4.24);
$objPHPExcel->getActiveSheet()->getColumnDimension("F")->setWidth(4.24);
$objPHPExcel->getActiveSheet()->getColumnDimension("G")->setWidth(4.24);
$objPHPExcel->getActiveSheet()->getColumnDimension("H")->setWidth(4.24);
$objPHPExcel->getActiveSheet()->getColumnDimension("I")->setWidth(4.24);
$objPHPExcel->getActiveSheet()->getColumnDimension("G")->setWidth(4.24);
$objPHPExcel->getActiveSheet()->getColumnDimension("H")->setWidth(4.24);
$objPHPExcel->getActiveSheet()->getColumnDimension("I")->setWidth(4.24);
$objPHPExcel->getActiveSheet()->getColumnDimension("J")->setWidth(4.24);
$objPHPExcel->getActiveSheet()->getColumnDimension("K")->setWidth(4.24);
$objPHPExcel->getActiveSheet()->getColumnDimension("L")->setWidth(4.24);
$objPHPExcel->getActiveSheet()->getColumnDimension("M")->setWidth(4.24);
$objPHPExcel->getActiveSheet()->getColumnDimension("N")->setWidth(4.24);
$objPHPExcel->getActiveSheet()->getColumnDimension("O")->setWidth(10.24);
$objPHPExcel->getActiveSheet()->getColumnDimension("P")->setWidth(10.24);
$objPHPExcel->getActiveSheet()->getColumnDimension("Q")->setWidth(11.24);
$objPHPExcel->getActiveSheet()->getColumnDimension("R")->setWidth(10.24);
$objPHPExcel->getActiveSheet()->getColumnDimension("S")->setWidth(10.24);
$objPHPExcel->getActiveSheet()->getColumnDimension("T")->setWidth(11.24);
$objPHPExcel->getActiveSheet()->getStyle('A4:T4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('A4:T4')->getAlignment()->setTextRotation(90);
$objPHPExcel->getActiveSheet()->getStyle("A4:T4")->getAlignment()->setWrapText(TRUE);
$objPHPExcel->getActiveSheet()->getStyle('A4:T4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
// Redirect output to a client’s web browser (Excel2007)

$objPHPExcel->setActiveSheetIndex(0)
->setCellValue("A5", "1")
->setCellValue("B5", "2")
->setCellValue("C5", "3")
->setCellValue("D5", "4")
->setCellValue("E5", "5")
->setCellValue("F5", "6")
->setCellValue("G5", "7")
->setCellValue("H5", "8")
->setCellValue("I5", "9")
->setCellValue("J5", "10")
->setCellValue("K5", "11")
->setCellValue("L5", "12")
->setCellValue("M5", "13")
->setCellValue("N5", "14")
->setCellValue("O5", "15")
->setCellValue("P5", "16")
->setCellValue("Q5", "17")
->setCellValue("R5", "18")
->setCellValue("S5", "19")
->setCellValue("T5", "20");
$i=5;$j=5;
while($row=mysql_fetch_array($result4)){
$i++;	
$j++;
$Appno=$row['Fileno'];	
$district=$row['districts'];	
$school_name=$row['schoolname'];
$org_name=$row['orgname'];
$community_mngmt=$row['community_mngmt'];
$Category=$row['Category'];
$Agency_cate=$row['Agency_cate'];
$school_type=$row['school_type'];
$Recongition_no=$row['Recongition_no'];
$Minority_Commision_date=$row['Minority_Commision_date'];
$agency_exp=$row['agency_exp'];
$school_exp=$row['school_exp'];
$Performance_report=$row['Performance_report'];
$school_studno=$row['school_studno'];
$Estimated_amount=$row['Estimated_amount'];
$source_investment=$row['source_investment'];
$Amount_Committed=$row['Amount_Committed'];
$Source_commitment=$row['Source_commitment'];
$Ground_grant=$row['Ground_grant'];
$Details_Proposal=$row['Details_Proposal'];

$Aid="Aided";
$registeration=$row['registration'];
$objPHPExcel->setActiveSheetIndex(0)
->setCellValue("A".$i, $Appno)
->setCellValue("B".$i, $district)
->setCellValue("C".$i, $school_name)
->setCellValue("D".$i, $org_name)
->setCellValue("E".$i, $community_mngmt)
->setCellValue("F".$i, $Category)
->setCellValue("G".$i, $Agency_cate)
->setCellValue("H".$i,$school_type)
->setCellValue("I".$i,$Recongition_no)
->setCellValue("J".$i, $Minority_Commision_date)
->setCellValue("K".$i, $agency_exp)
->setCellValue("L".$i, $school_exp)
->setCellValue("M".$i, $Performance_report)
->setCellValue("N".$i, $school_studno)
->setCellValue("O".$i, $Estimated_amount)
->setCellValue("P".$i, $source_investment)
->setCellValue("Q".$i, $Amount_Committed)
->setCellValue("R".$i, $Source_commitment)
->setCellValue("S".$i, $Ground_grant)
->setCellValue("T".$i, $Details_Proposal);

// $objPHPExcel->getActiveSheet()->getRowDimension('6')->setRowHeight(150);
$x='A'.$j;
$y='T'.$j;
$objPHPExcel->getActiveSheet()->getStyle($x.':'.$y)->getAlignment()->setWrapText(TRUE);
$objPHPExcel->getActiveSheet()->getStyle($x.':'.$y)->getAlignment()->setTextRotation(90);
$objPHPExcel->getActiveSheet()->getStyle('C'.$i.':D'.$i)->getAlignment()->setTextRotation(0);
$objPHPExcel->getActiveSheet()->getRowDimension($j)->setRowHeight(150);
// $objPHPExcel->getActiveSheet()->getStyle('A7:T7')->getAlignment()->setTextRotation(90);
$objPHPExcel->getActiveSheet()->getStyle($x.':'.$y)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
// $objPHPExcel->getActiveSheet()->getColumnDimension("D")->setWidth(2.24);


	// $_row_number = 7;
	// $excel->getActiveSheet()->getRowDimension(7)->setRowHeight(-1);
// $objPHPExcel->getActiveSheet()->mergeCells("A6:T6");
}
// 
 // $objPHPExcel->getActiveSheet()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A3);
// $objPHPExcel->getActiveSheet()->setFitToWidth(true);
// $objPHPExcel->getActiveSheet()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="IDMI.xlsx"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;
