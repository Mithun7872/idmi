<?php
include_once("inc/pass_change.inc.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link rel="stylesheet" type="text/css" href="style.css" />
<script src="JSCal2/js/jscal2.js"></script>
	<script src="JSCal2/js/lang/en.js"></script>
	<link rel="stylesheet" type="text/css" href="JSCal2/css/jscal2.css" />
	<link rel="stylesheet" type="text/css" href="JSCal2/css/border-radius.css" />
	<link rel="stylesheet" type="text/css" href="JSCal2/css/steel/steel.css" />

<script src="JSCal2/js/gen_validatorv31.js" type="text/javascript"></script>
</head>

<body>
<div id="container">

<div id="header">
	<div id="header_title">SCHEME FOR INFRASTRUCTURE DEVELOPMENT PRIVATE AIDED/UNAIDED<br />MINORITY INSTITUTES(IDMI)-(ELEMENTARY SECONDARY/SENIOR<br />SECONDARY SCHOOLS)</div>
  
  
</div>
<div id="menubar"><ul>
     	<li><a href="index.php">Home</a></li>
        <li><a href="input.php">New Application</a></li>
         <li><a href="edit.php">Edit Application</a></li>
          <li><a href="pdfgeneration.php">Generate pdf</a></li>
          <li><a href="part2.php">part2</a></li>
          <li><a href="login.php">Login</a></li>
        </ul></div>
<div id="body">


<div id="middle">
<form name="frmchangepsd" method="post" action="">
	<table align="center">
	<td>
		<fieldset style="width:600px;">
<table width="600" cellspacing="10" cellpadding="0" id="tab" align="center">
  <tr>
    <td colspan="3" align="center"><h3>Change Password</h3></td>
  </tr>
  <tr>
    <td width="211"><strong>Old Password</strong></td>
    <td width="79" align="center"><strong>:</strong></td>
    <td width="308"><input type="text" name="old" id="old" value=""></td>
  </tr>
  <tr>
    <td><strong>New Password</strong></td>
    <td align="center"><strong>:</strong></td>
    <td><input type="password" name="new" id="new" value=""></td>
  </tr>
  <tr>
    <td><strong>Confirm Password</strong></td>
    <td align="center"><strong>:</strong></td>
    <td><input type="password" name="confirm" id="confirm" value=""></td>
  </tr>
  <tr>
    <td colspan="3" align="center"><input type="submit" name="change" id="change" value="Change"></td>
  </tr>
  <tr>
    <td colspan="3" align="center"><span id="msg"><?php echo $msg;?></span></td>
  </tr>
</table>
</fieldset>	
	</td>	
	</table>
  
</form>
</div>

</div>
<div id="footerouter">
<div id="footer">© All Rights Reserved. IT@School, General Education Department, Govt of Kerala. </div>
</div>
</div>
</body>
<script language="javascript" type="text/javascript">
 var frmvalidator = new Validator("frmchangepsd");
 frmvalidator.addValidation("old","req","Please Enter the Old Passwprd !");
  var frmvalidator = new Validator("frmchangepsd");
 frmvalidator.addValidation("new","req","Please Enter the New Passwprd  !");
  var frmvalidator = new Validator("frmchangepsd");
 frmvalidator.addValidation("confirm","req","Please Enter the New Passwprd again !");
 </script>
</html>